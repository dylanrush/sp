import time
import fluidsynth
import random

fs = fluidsynth.Synth()
fs.start()

sfid = fs.sfload("/home/dylan/Downloads/Sine Wave.sf2")
fs.program_select(0, sfid, 0, 0)

diminishedSeventh=[0,3,6,9]
halfDiminishedSeventh=[0,3,6,10]
minorSeventh=[0,3,7,10]
minorMajorSeventh=[0,3,7,11]
augmentedSeventh=[0,4,8,10]
dominantSeventh=[0,4,7,10]
majorSeventh=[0,4,7,11]
augmentedMajorSeventh=[0,4,8,11]

augmentedTriad=[0,4,8]
majorTriad=[0,4,7]
minorTriad=[0,3,7]
diminishedTriad=[0,3,6]

seventhChords = [diminishedSeventh, halfDiminishedSeventh, minorSeventh, minorMajorSeventh, augmentedSeventh, dominantSeventh, majorSeventh, augmentedMajorSeventh]

def chordOn(midibase, notes, midisteps):
  if(notes % 2 == 1):
    fs.noteon(0, midibase+midisteps[0], 30)
  if((notes >> 1)%2 == 1):
    fs.noteon(0, midibase+midisteps[1], 30)
  if((notes >> 2)%2 == 1):
    fs.noteon(0, midibase+midisteps[2], 30)
  if((notes >> 3)%2 == 1):
    fs.noteon(0, midibase+midisteps[3], 30)

def chordOff(midibase, notes, midisteps):
  if(notes % 2 == 1):
    fs.noteoff(0, midibase+midisteps[0])
  if((notes >> 1)%2 == 1):
    fs.noteoff(0, midibase+midisteps[1])
  if((notes >> 2)%2 == 1):
    fs.noteoff(0, midibase+midisteps[2])
  if((notes >> 3)%2 == 1):
    fs.noteoff(0, midibase+midisteps[3])


C1=24

# diminished
# C Eb Gb A
# major
# C E G B

steps=majorSeventh

for midibase in range(C1, 8*12, 12):
  notes=1+2+8
  
  chordOn(midibase, notes, steps)
  time.sleep(0.5)
  chordOff(midibase, notes, steps)

while True:
  notes = 0
  notes += random.choice([0,1])
  notes += random.choice([0,2])
  notes += random.choice([0,4])
  notes += random.choice([0,8])
  octave = random.randint(2, 3)
  midibase = C1+octave*12
  chordOn(midibase, notes, steps)
  time.sleep(0.5)
  chordOff(midibase, notes, steps)

time.sleep(1.0)


time.sleep(1.0)

fs.delete()
